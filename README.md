# Rohankid1's Website Source Code

This repository contains the source code for building the website
This website is hosted through Vercel, and can be found [here](https://rohankid1.vercel.app)

# Building (brief guide)

For hosting on Vercel, just fork this repo, login/register onto `https://vercel.com`, and create from Git repository.

1. First, clone and download this repository.
2. CD into the repo from your terminal
3. Build the dependencies (`npm install`, `pnpm install`, `yarn`)
4. For starting a development server, run `npm run dev`.
   - For building a production version:
     `npm run build`
   - For previewing the production build:
     `npm run preview`
