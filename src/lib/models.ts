export interface Project {
	id: string;
	name: string;
	description: string;
	html_url: string;
	language: string;
	fork: boolean;
}

export interface PageLink {
	url: string;
	value: string;
}

export interface Post {
	id: number;
	title: string;
	body: string;
}
