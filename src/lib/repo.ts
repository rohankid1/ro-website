import type { Project } from './models';

export const getRepositories = async (): Promise<Project[]> => {
	const res = await fetch('https://api.github.com/users/rohankid1/repos');
	const data: Project[] = await res.json();
	return data;
};