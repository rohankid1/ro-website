import type { Post } from './models';
import { type Writable, writable } from 'svelte/store';

export const posts: Writable<Post[]> = writable([]);

export const getPosts = async (): Promise<Post[]> => {
	const response = await fetch(`https://jsonplaceholder.typicode.com/posts`);
	const jsonRes: Promise<Post[]> = await response.json();
	const res = await jsonRes;
	posts.update((currItems) => [...currItems, ...res]);

	return res;
};
