import { type Readable, readable } from 'svelte/store';
import type { PageLink } from './models';

// Convenient access on other pages that may need it.
export const pages: Readable<PageLink[]> = readable([
	{ url: '/', value: 'Home' },
	{ url: '/projects', value: 'Projects' },
	{ url: '/posts', value: 'Posts' },
	{ url: '/open-source', value: 'Open Source' }
]);
