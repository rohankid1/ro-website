import { error } from '@sveltejs/kit';

export function load({ params }) {
	// throw error if the id parameter is not a number or invalid.
	if (!parseInt(params.id)) throw error(404, { message: 'Not found' });
}
